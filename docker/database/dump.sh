#!/bin/bash

if [ $# -eq 0 ]; then
	echo " >>> Forma de ejecución: sh $0 <db_name (sub carpeta)> <user_name> <password>.";
	exit;
fi

if [$1 -eq '']; then
	echo ' >>> El nombre de la base de datos (sub carpeta) es requerido.';
	exit;
fi

if [$2 -eq '']; then
	echo ' >>> El usuario es requerido.';
	exit;
fi

if [$3 -eq '']; then
	echo ' >>> La password es requerida.';
	exit;
fi

FILES="$1/*.sql"

for f in $FILES
do
	mysql -u$2 -p$3 $1 < $f;
done
