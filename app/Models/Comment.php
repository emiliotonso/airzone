<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user',
        'datetime',
        'content',
    ];

    public function post()
    {
        return $this->hasOneThrough(
            Post::class,
            CommentPost::class,
            'comment',
            'id',
            'id',
            'blog'
        );
    }

    public function writer()
    {
        return $this->belongsTo(User::class, 'user');
    }
}
