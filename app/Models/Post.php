<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Post extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user',
        'title',
        'slug',
        'marks',
        'picture',
        'short_content',
        'content',
        'comment',
        'pending',
        'public',
        'active',
    ];

    public function category()
    {
        return $this->hasOneThrough(
            Category::class,
            CategoryPost::class,
            'blog',
            'id',
            'id',
            'category'
        );
    }

    public function comments()
    {
        return $this->belongsToMany(Comment::class, 'comment_post', 'blog', 'comment');
        // return $this->hasManyThrough(
        //     Comment::class,
        //     CommentPost::class,
        //     'blog',
        //     'id',
        //     'id',
        //     'comment'
        // );
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user');
    }

    public function writers()
    {
        return Cache::remember('writers_' . $this->id, 600, function () {
            return User::select('users.*')
                ->join('comments', 'users.id', '=', 'comments.user')
                ->join('comment_post', 'comments.id', '=', 'comment_post.comment')
                ->where('comment_post.blog', $this->id)
                ->get()
            ;
        });
    }
}
