<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'short_content' => $this->short_content,
	        'owner' => new UserResource($this->whenLoaded('owner')),
            'users' => new UsersResource($this->writers()),
            'comments' => new CommentsResource($this->whenLoaded('comments')),
        ];
    }
}
