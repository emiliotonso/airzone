<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class CategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parent_id' => 'integer|nullable|exists:categories,id',
            'name' => 'required|string|min:1|max:128',
            'slug' => 'required|string|min:1|max:128',
            'visible' => 'boolean',
        ];
    }

	/**
	 * Configure the validator instance.
	 *
	 * @param  \Illuminate\Validation\Validator  $validator
	 * @return void
	 */
    public function withValidator($validator)
	{
		$validator->after(function ($validator) {
			if ($desconocidos = Arr::except($this->validationData(), array_keys($this->rules()))) {
				foreach ($desconocidos as $key => $value) {
					$validator->errors()->add($key, 'Unknown Parameter');
				}
			}
		});
	}
}
