<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommentPostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $commentsPots = [
            [
                'comment' => 1,
                'blog' => 1,
            ],
            [
                'comment' => 2,
                'blog' => 3,
            ],
            [
                'comment' => 3,
                'blog' => 3,
            ],
        ];

        $commentsPotsDataBase = DB::table('comment_post')->get();

        foreach ($commentsPots as $commentPot) {
            $register = $commentsPotsDataBase->firstWhere([
                ['comment', $commentPot['comment']],
                ['blog', $commentPot['blog']],
            ]);

			if (is_object($register)) {
				$register->update($commentPot);

				continue;
			}

			DB::table('comment_post')->insert($commentPot);
		}
    }
}
