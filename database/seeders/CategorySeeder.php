<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'parent_id' => null,
                'name' => 'Laravel',
                'slug' => 'laravel',
                'visible' => 1
            ],
            [
                'parent_id' => null,
                'name' => 'VueJs',
                'slug' => 'vuejs',
                'visible' => 1
            ],
        ];

        $caterogiesDataBase = Category::get([
			'parent_id',
			'name',
			'slug',
			'visible',
		]);

        foreach ($categories as $category) {
            $register = $caterogiesDataBase->firstWhere('slug', $category['slug']);

			if (is_object($register)) {
				$register->update($category);

				continue;
			}

			Category::create($category);
		}
    }
}
