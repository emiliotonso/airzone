<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryPostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoriesPots = [
            [
                'category' => 1,
                'blog' => 1,
            ],
            [
                'category' => 2,
                'blog' => 3,
            ],
        ];

        $categoriesPotsDataBase = DB::table('category_post')->get();

        foreach ($categoriesPots as $categoryPost) {
            $register = $categoriesPotsDataBase->firstWhere([
                ['category', $categoryPost['category']],
                ['blog', $categoryPost['blog']],
            ]);

			if (is_object($register)) {
				$register->update($categoryPost);

				continue;
			}

			DB::table('category_post')->insert($categoryPost);
		}
    }
}
