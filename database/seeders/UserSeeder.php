<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'username' => 'usuario_1',
                'full_name' => 'Usuario 1',
            ],
            [
                'username' => 'usuario_2',
                'full_name' => 'Usuario 2',
            ],
            [
                'username' => 'usuario_3',
                'full_name' => 'Usuario 3',
            ],
        ];

        $usersDataBase = User::get([
			'username',
			'full_name',
		]);

        foreach ($users as $user) {
            $register = $usersDataBase->firstWhere('username', $user['username']);

			if (is_object($register)) {
				$register->update($user);

				continue;
			}

			User::create($user);
		}
    }
}
