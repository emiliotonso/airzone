<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('posts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->charset = 'utf8mb4';
			$table->collation = 'utf8mb4_unicode_ci';

            $table->id();
            $table->foreignId('user')->nullable()->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('title', 128);
            $table->string('slug', 128);
            $table->mediumText('marks')->nullable();
            $table->string('picture', 128)->nullable();
            $table->text('short_content');
            $table->longtext('content')->nullable();
            $table->tinyInteger('comment')->default(0);
            $table->tinyInteger('pending')->default(0);
            $table->tinyInteger('public')->default(1);
            $table->tinyInteger('active')->default(1);
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('posts');
        Schema::enableForeignKeyConstraints();
    }
}
