<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('comment_post', function (Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->charset = 'utf8mb4';
			$table->collation = 'utf8mb4_unicode_ci';

            $table->id();
			$table->foreignId('comment')->nullable()->references('id')->on('comments')->onDelete('cascade')->onUpdate('cascade');
			$table->foreignId('blog')->nullable()->references('id')->on('posts')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();

            $table->unique(['comment', 'blog'], 'CommentBlogUK');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('comment_post');
        Schema::enableForeignKeyConstraints();
    }
}
