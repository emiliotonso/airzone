<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('categories')->group(function () {
    Route::post('/', 'CategoryController@store')->name('categories.store');

    Route::prefix('{id}')->group(function () {
        Route::put('/', 'CategoryController@update')->name('categories.update');
        Route::delete('/', 'CategoryController@destroy')->name('categories.destroy');
        Route::get('/', 'CategoryController@show')->name('categories.show');
    });
});

Route::prefix('posts')->group(function () {
    Route::prefix('{id}')->group(function () {
        Route::get('/', 'PostController@show')->name('posts.show');
    });
});
